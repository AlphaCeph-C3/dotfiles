#!/bin/sh
#
# ~/.bashrc
#


### EXPORT
# export TERM="xterm-256color"                      # getting proper colors
# export HISTCONTROL=ignoredups:erasedups           # no duplicate entries
# export VISUAL=nvim                                # to set the default editor to neovim
# export EDITOR="$VISUAL"

# ### "bat" as manpager
# export MANPAGER="sh -c 'col -bx | bat -l man -p'"


# neofetch


# [[ $- != *i* ]] && return



# bash completion
# [ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

# # Change the window title of X terminals
# case ${TERM} in
# 	xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|alacritty|st|yakuake|interix|konsole*)
# 		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
# 		;;
# 	screen*)
# 		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
# 		;;
# esac



# xhost +local:root > /dev/null 2>&1

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
### SHOPT
# shopt -s autocd # change to named directory
# shopt -s cdspell # autocorrects cd misspellings
# shopt -s cmdhist # save multi-line commands in history as single line
# shopt -s dotglob
# shopt -s histappend # do not overwrite history
# shopt -s expand_aliases # expand aliases
# shopt -s checkwinsize # checks term size when bash regains control


# export QT_SELECT=4


#
# # ex - archive extractor
# # usage: ex <file>
# ex ()
# {
#   if [ -f $1 ] ; then
#     case $1 in
#       *.tar.bz2)   tar xjf $1   ;;
#       *.tar.gz)    tar xzf $1   ;;
#       *.bz2)       bunzip2 $1   ;;
#       *.rar)       unrar x $1     ;;
#       *.gz)        gunzip $1    ;;
#       *.tar)       tar xf $1    ;;
#       *.tbz2)      tar xjf $1   ;;
#       *.tgz)       tar xzf $1   ;;
#       *.zip)       unzip $1     ;;
#       *.Z)         uncompress $1;;
#       *.7z)        7z x $1      ;;
#       *)           echo "'$1' cannot be extracted via ex()" ;;
#     esac
#   else
#     echo "'$1' is not a valid file"
#   fi
# }



# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
# __conda_setup="$('$HOME/mambaforge/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
# if [ $? -eq 0 ]; then
#     eval "$__conda_setup"
# else
#     if [ -f "$HOME/mambaforge/etc/profile.d/conda.sh" ]; then
#         . "$HOME/mambaforge/etc/profile.d/conda.sh"
#     else
#         export PATH="$HOME/mambaforge/bin:$PATH"
#     fi
# fi
# unset __conda_setup

# if [ -f "$HOME/mambaforge/etc/profile.d/mamba.sh" ]; then
#     . "$HOME/mambaforge/etc/profile.d/mamba.sh"
# fi
# <<< conda initialize <<<

#ignore upper and lowercase when TAB completion
# bind "set completion-ignore-case on"

# time the startup time of the shell
timeshell(){
    for i in $(seq 1 10); do
        time "$1" -i -c exit;
    done
}

HISTFILE=~/.zsh_history
export HISTSIZE=100000
export SAVEHIST=100000


# env variable for python to stop writing the byte codes which is .pyc or the folder __pycache__
export PYTHONDONTWRITEBYTECODE=abc

export HISTORY_SUBSTRING_SEARCH_PREFIXED=true

# For unique history command
export HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE=true

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# fzf setup
#combined these to lines into a file called .fzf.bash
#source /usr/share/fzf/key-bindings.bash
#source /usr/share/fzf/completion.bash



# fuzzy finder setup
#[ -f ~/.fzf.bash ] && source ~/.fzf.bash
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

FD_OPTIONS='--follow --exclude .git --exclude node_modules'

export FZF_DEFAULT_COMMAND="fd --type f --type l $FD_OPTIONS"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_OPTS="
  --preview 'bat -n --color=always {}'
  --bind 'ctrl-/:change-preview-window(down|hidden|)'"
export FZF_ALT_C_COMMAND='fd --type d $FD_OPTIONS'
export FZF_ALT_C_OPTS="--preview 'tree -C {}'"


# load dotfiles which are required by the bashrc file:
# proper way to define an array in bash and zsh
files=("aliases/git.bash" "aliases/general.bash")

for file in $files; do
#  file=~/."$file"
  if [[ -r ~/."$file"  &&  -f ~/."$file" ]]; then
      source ~/."$file";
  fi
done;
unset file;


# +++++++++++++++++++++++ necesarry according to dt change after learning bash scripting +++++++++++++++++++++++++++++++++++++++++++++++++
# colors() {
# 	local fgc bgc vals seq0
#
# 	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
# 	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
# 	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
# 	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"
#
# 	# foreground colors
# 	for fgc in {30..37}; do
# 		# background colors
# 		for bgc in {40..47}; do
# 			fgc=${fgc#37} # white
# 			bgc=${bgc#40} # black
#
# 			vals="${fgc:+$fgc;}${bgc}"
# 			vals=${vals%%;}
#
# 			seq0="${vals:+\e[${vals}m}"
# 			printf "  %-9s" "${seq0:-(default)}"
# 			printf " ${seq0}TEXT\e[m"
# 			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
# 		done
# 		echo; echo
# 	done
# }

#use_color=true

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
#safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
#match_lhs=""
#[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
#[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
#[[ -z ${match_lhs}    ]] \
#	&& type -P dircolors >/dev/null \
#	&& match_lhs=$(dircolors --print-database)
#[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true

#if ${use_color} ; then
#	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
#	if type -P dircolors >/dev/null ; then
#		if [[ -f ~/.dir_colors ]] ; then
#			eval $(dircolors -b ~/.dir_colors)
#		elif [[ -f /etc/DIR_COLORS ]] ; then
#			eval $(dircolors -b /etc/DIR_COLORS)
#		fi
#	fi
#
#	if [[ ${EUID} == 0 ]] ; then
#		PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
#	else
#		PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\$\[\033[00m\] '
#	fi
#
#	alias ls='ls --color=auto'
#	alias grep='grep --colour=auto'
#	alias egrep='egrep --colour=auto'
#	alias fgrep='fgrep --colour=auto'
#else
#	if [[ ${EUID} == 0 ]] ; then
#		# show root@ when we don't have colors
#		PS1='\u@\h \W \$ '
#	else
#		PS1='\u@\h \w \$ '
#	fi
#fi

#unset use_color safe_term match_lhs sh

#alias cp="cp -i"                          # confirm before overwriting something
#alias df='df -h'                          # human-readable sizes
#alias free='free -m'                      # show sizes in MB
#alias np='nano -w PKGBUILD'
#alias more=less
eval $(thefuck --alias)

alias cd="z"

. "$HOME/.cargo/env"

. "$HOME/.local/bin/env"
#
# fnm
FNM_PATH="/home/kukka/.local/share/fnm"
if [ -d "$FNM_PATH" ]; then
  export PATH="/home/kukka/.local/share/fnm:$PATH"
  #eval "`fnm env --use-on-cd --shell bash`"
  eval "`fnm env --use-on-cd --shell zsh`"
fi


export PATH=$PATH:/usr/local/go/bin

eval "$(starship init zsh)"
eval "$(zoxide init zsh)"

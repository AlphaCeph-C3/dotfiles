" Set no compatibility
set nocompatible

" New lines inherit the indentation of previous lines
set autoindent

" Convert tabs to Spaces
set expandtab

" Insert “tabstop” number of spaces when the “tab” key is pressed.
set smarttab

" Indent using four spaces.
set tabstop=4

" Enable Search highlighting
set hlsearch

" Ignore case when searching
set ignorecase

" Incremental search that shows partial matches
set incsearch

" Automatically switch search to case-sensitive when search query contains an uppercase letter.
set smartcase

" Enable line wrapping
set wrap

" Enable syntax highlighting.
syntax enable

" Avoid wrapping a line in the middle of a word.
set linebreak

" Use an encoding that supports unicode.
set encoding=utf-8

" Always try to show a paragraph’s last line.
set display+=lastline



" Always show cursor position
set ruler

" Display command line’s tab complete options as a menu
set wildmenu

" Change color scheme.

" Show line number on the sidebar
set number

" Show line number on the current line and relative numbers on all other lines.
set relativenumber

" Set commands to save in history
set history=1000

" Do not save backup files.
set nobackup

" Do not let cursor scroll below or above N number of lines when scrolling.
set scrolloff=10

" Show partial command you type in the last line of the screen.
set showcmd

" Show the mode you are on the last line.
set showmode

" Show matching words during a search.
set showmatch

" Make wildmenu behave like similar to Bash completion.
set wildmode=list:longest

" There are certain files that we would never want to edit with Vim.
" Wildmenu will ignore files with these extensions.
set wildignore=*.docx,*.jpg,*.png,*.gif,*.pdf,*.pyc,*.exe,*.flv,*.img,*.xlsx


" STATUS LINE ------------------------------------------------------------ {{{

" Clear status line when vimrc is reloaded.
set statusline=

" Status line left side.
set statusline+=\ %F\ %M\ %Y\ %R

" Use a divider to separate the left side from the right side.
set statusline+=%=

" Status line right side.
"set statusline+=\ ascii:\ %b\ hex:\ 0x%B\ row:\ %l\ col:\ %c\ percent:\ %p%%

" Show the status on the second to last line.
set laststatus=2


" keybindings for no hightlight search
nnoremap <esc><esc> :noh<return><esc>

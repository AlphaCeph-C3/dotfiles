# dotfiles

These are my dotfiles which I edited with my preference in mind.

## These dotfiles include

- NeoVim config
- bash config
- aliases for commands
- conky setup

## Steps to setup

1. (Optional) if you have a freshly installed arch linux system then run the `arch-pacman.sh`

   > `use: ./arch-pacman.sh`

   warning: do not use it if you don't understand what it is doing

2. Use the `smylinking.sh` script to create the symlinks in your `$HOME`.

   > `use: ./symlinking.sh <path/to/your/home/directory>`

3. Install conky(used for system monitoring), starship(used to setup terminal prompt)

   - After installing conky run the command
     > `conky <path/to/your/conky.conkyrc/file>`
   - If starship is installed then no need to do anything as you have created the symlink

4. Install Alacritty for terminal application you already have the config file.

## Things to do

- Configure qtile and polybar and update the repo.

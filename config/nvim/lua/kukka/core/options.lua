vim.g.netrw_liststyle = 3
vim.g.loaded_perl_provider = 0

local opt = vim.opt

opt.guicursor = ""

opt.number = true
opt.relativenumber = true

-- things to do about the tabs
opt.tabstop = 4
opt.softtabstop = 4
opt.shiftwidth = 4
opt.expandtab = true
opt.autoindent = true

opt.smartindent = true
opt.smartcase = true
opt.ignorecase = true

opt.wrap = false

opt.cursorline = true

opt.incsearch = true

opt.termguicolors = true

opt.scrolloff = 10
opt.signcolumn = "yes"

opt.colorcolumn = "80"

-- Sync clipboard between OS and Neovim
-- Schedule the setting after 'UiEnter' because it can increase the startup time'
-- Remove this option if you want your OS clipboard to remain independent
-- See `:help 'clipboard'`
vim.schedule(function()
	opt.clipboard = "unnamedplus"
end)

opt.showcmd = true
opt.showmode = false -- don't show the mode, since it's already in the status line
opt.undofile = true

opt.mouse = "a"
opt.laststatus = 3

opt.updatetime = 250

opt.timeoutlen = 500

-- sets how neovim will display certain whitespace characters in the editor.
-- see `:help 'list'`
-- see `:help 'listchars'`
opt.list = true
opt.listchars = { tab = "» ", trail = "·", nbsp = "␣" }

opt.inccommand = "split"

-- turn off swapfile
opt.swapfile = false

-- split windows
opt.splitright = true
opt.splitbelow = true

-- backspace
opt.backspace = "indent,eol,start"

-- background
opt.background = "dark"

-- the keymaps which are required for the setup
--
local keymap = vim.keymap.set

--keymap("n", "<space><space>x", "<cmd>source %<CR>", { desc = "Source (run) the current file or buffer." })
--keymap("n", "<space>x", ":.lua<CR>",{ desc = "Run a single line of lua in the current buffer or file." })
--keymap("v", "<space>x", ":lua<CR>", { desc = "Run the entire selection of lines in visual of lua code." })

keymap("t", "<Esc><Esc>", "<C-\\><C-n>", { desc = "Exit terminal mode" })
keymap("n", "<Esc>", "<cmd>nohlsearch<CR>", { desc = "Clear the highlights for the search" })

-- Diagnostic keymaps
keymap("n", "<space>q", vim.diagnostic.setloclist, { desc = "Open diagnostic [Q]uickfix list" })

keymap("n", "<left>", "<cmd>echo 'Use h to move!!'<CR>")
keymap("n", "<right>", "<cmd>echo 'Use l to move!!'<CR>")
keymap("n", "<up>", "<cmd>echo 'Use k to move!!'<CR>")
keymap("n", "<down>", "<cmd>echo 'Use j to move!!'<CR>")

-- increment/decrement numbers
keymap("n", "<leader>+", "<C-a>", { desc = "Increment number" }) -- increment
keymap("n", "<leader>-", "<C-x>", { desc = "Decrement number" }) -- decrement

-- scroll the cursor having it placed in the center of the screen
keymap("n", "<C-d>", "<C-d>zz", { desc = "Scroll down with the cursor in the middle" })
keymap("n", "<C-u>", "<C-u>zz", { desc = "Scroll up with the cursor in the middle" })

-- window management
--keymap("n", "<leader>sv", "<C-w>v", { desc = "Split window vertically" }) -- split window vertically
--keymap("n", "<leader>sh", "<C-w>s", { desc = "Split window horizontally" }) -- split window horizontally
--keymap("n", "<leader>se", "<C-w>=", { desc = "Make splits equal size" }) -- make split windows equal width & height
--keymap("n", "<leader>sx", "<cmd>close<CR>", { desc = "Close current split" }) -- close current split window

-- tab management
keymap("n", "<leader>to", "<cmd>tabnew<CR>", { desc = "[O]pen new tab" }) -- open new tab
keymap("n", "<leader>tx", "<cmd>tabclose<CR>", { desc = "[] Close current tab" }) -- close current tab
keymap("n", "<leader>tn", "<cmd>tabn<CR>", { desc = "Go to [N]ext tab" }) --  go to next tab
keymap("n", "<leader>tp", "<cmd>tabp<CR>", { desc = "Go to [P]revious tab" }) --  go to previous tab
keymap("n", "<leader>tf", "<cmd>tabnew %<CR>", { desc = "[] Open current buffer in new tab" }) --  move current buffer to new tab

-- buffer management
keymap("n", "<leader>bd", "<cmd>bd<CR>", { desc = "[] Close current buffer" })
keymap("n", "<leader>bn", "<cmd>bn<CR>", { desc = "Go to [N]ext buffer" })
keymap("n", "<leader>bp", "<cmd>bp<CR>", { desc = "Go to [P]revious buffer" })


-- Keybinds to make split navigation easier.
--  Use CTRL+<hjkl> to switch between windows
--
--  See `:help wincmd` for a list of all window commands

--keymap('n', '<C-h>', '<C-w><C-h>', { desc = 'Move focus to the left window' })
--keymap('n', '<C-l>', '<C-w><C-l>', { desc = 'Move focus to the right window' })
--keymap('n', '<C-j>', '<C-w><C-j>', { desc = 'Move focus to the lower window' })
--keymap('n', '<C-k>', '<C-w><C-k>', { desc = 'Move focus to the upper window' })

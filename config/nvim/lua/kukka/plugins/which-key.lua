return {
	{
		"folke/which-key.nvim",
		event = "VeryLazy",
		init = function()
			vim.o.timeout = true
			vim.o.timeoutlen = 500
		end,
		opts = {
			spec = {
				{ "<leader>s", group = "[S]earch" },
				{ "<leader>e", group = "[E]xplorer" },
				{ "<leader>t", group = "[T]abs" },
				{ "<leader>w", group = "[W]orkspaces" },
				{ "<leader>x", group = "[X]trouble" },
			},
		},
	},
}

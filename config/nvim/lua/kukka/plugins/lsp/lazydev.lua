return {
	{
		-- lazydev configures Lua LSP for your Neovim config, runtime and plugins
		-- used for completion, annotatinos and signatures of Neovim apis
		"folke/lazydev.nvim",
		ft = "lua",
		opts = {
			library = {
				-- laod luvit types when the `vim.uv` word is found
				{ path = "${3rd}/lua/library", words = { "vim%.uv" } },
			},
		},
	},
}

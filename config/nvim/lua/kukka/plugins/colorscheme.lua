return {
	{
		"ellisonleao/gruvbox.nvim",
		priority = 1000,
		config = function()
			local transparent = false
			-- Default options:
			require("gruvbox").setup({
				terminal_colors = true, -- add neovim terminal colors
				undercurl = true,
				underline = true,
				bold = true,
				italic = {
					strings = true,
					emphasis = true,
					comments = true,
					operators = false,
					folds = true,
				},
				strikethrough = true,
				invert_selection = false,
				invert_signs = false,
				invert_tabline = false,
				invert_intend_guides = false,
				inverse = true, -- invert background for search, diffs, statuslines and errors
				contrast = "", -- can be "hard", "soft" or empty string
				palette_overrides = {},
				overrides = {},
				dim_inactive = false,
				transparent_mode = transparent,
			})
			vim.cmd("colorscheme gruvbox")
		end,
	},
}

-- return {
-- 	{
-- 		"folke/tokyonight.nvim",
-- 		priority = 1000,
-- 		config = function()
-- 			local transparent = false
-- 			require("tokyonight").setup({
-- 				style = "night",
-- 				transparent = transparent,
-- 				styles = {
-- 					siderbars = transparent and "transparent" or "dark",
-- 					floats = transparent and "transparent" or "dark",
-- 				},
-- 			})
-- 			vim.cmd([[ colorscheme tokyonight ]])
-- 		end,
-- 	},
-- }

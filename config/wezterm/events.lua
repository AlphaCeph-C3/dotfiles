-- Pull in the Wezterm API
local wezterm = require "wezterm"
local mux = wezterm.mux

wezterm.on("gui-startup", function(cmd)
  local screen = wezterm.gui.screens().active
  local _, _, window = mux.spawn_window(cmd or {})
  local gui = window:gui_window()
  local width = 0.7 * screen.width
  local height = 0.7 * screen.height
  gui:set_inner_size(width, height)
  gui:set_position((screen.width - width)/2, (screen.height - height)/2)
end)

-- -- as the name suggest
-- local function get_current_working_dir(tab)
--   local current_dir = tab.active_pane and tab.active_pane.current_working_dir or { file_path = "" }
--   local HOME_DIR = string.format("file://%s", os.getenv("HOME"))
--
--   -- have absolutely no idea what is happening here
--   -- but understood that you are replacing the entire file path with only the file name
--   return current_dir == HOME_DIR and "." or string.gsub(current_dir.file_path, "(.*[/\\])(.*)", "%2 ")
-- end
--
-- -- tab title
-- wezterm.on("format-tab-title", function(tab, tabs, panes, config, hover, max_width)
--   local has_unseen_output = false
--   if not tab.is_active then
--     for _, pane in ipairs(tab.panes) do
--       if pane.has_unseen_output then
--         has_unseen_output = true
--         break
--       end
--     end
--   end
--
--   local cwd = wezterm.format({
--     { Attribute = { Intensity = "Bold" } },
--     { Text = get_current_working_dir(tab) },
--   })
--
--   local title = string.format("[%s] %s", tab.tab_index + 1, cwd)
--
--   if has_unseen_output then
--     return {
--       { Foreground = { Color = "#8866bb" } },
--       { Text = title },
--     }
--   end
--
--   return {
--     { Text = title },
--   }
-- end)
--
-- workspaces
wezterm.on("upadate_status", function(window, pane)
  window:set_right_status(window:active_workspace())
end)

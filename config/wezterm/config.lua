local wezterm = require("wezterm")
local config = {}
-- local keys = require("keybinds")

local config_dir = wezterm.config_dir

-- local function mergeTables(t1, t2)
-- 	for key, value in pairs(t2) do
-- 		t1[key] = value
-- 	end
-- end

if wezterm.config_builder then
	config = wezterm.config_builder()
end
--
-- ssh hosts from ~/.ssh/config
local ssh_domains = {}
for host, config_ in pairs(wezterm.enumerate_ssh_hosts()) do
	table.insert(ssh_domains, {
		-- the name can be anything you want; we're just using the hostname
		name = host,
		-- remote_address must be set to `host` for the ssh config to apply to it
		remote_address = host,

		-- if you don't have wezterm's mux server installed on the remote
		-- host, you may wish to set multiplexing = "None" to use a direct
		-- ssh connection that supports multiple panes/tabs which will close
		-- when the connection is dropped.

		-- multiplexing = "None",

		-- if you know that the remote host has a posix/unix environment,
		-- setting assume_shell = "Posix" will result in new panes respecting
		-- the remote current directory when multiplexing = "None".
		assume_shell = "Posix",
	})
end

config.automatically_reload_config = true
config.window_close_confirmation = "NeverPrompt"
config.window_decorations = "RESIZE"
config.use_fancy_tab_bar = false
config.warn_about_missing_glyphs = false
config.scrollback_lines = 10000
config.enable_scroll_bar = false
-- config.color_scheme = "tokyonight_night"
config.color_scheme = "GruvboxDarkHard"
config.tab_bar_at_bottom = true
config.window_padding = {
	left = 0,
	right = 0,
	top = 0,
	bottom = 0,
}
-- config.hide_tab_bar_if_only_one_tab = true
-- config.tab_and_split_indices_are_zero_based = true
-- config.default_prog = { "/usr/bin/zsh" }
-- config.font_size = 11
config.font = wezterm.font_with_fallback({
	-- { family = "CaskaydiaCove Nerd Font Mono", weight = "Medium" },
	-- { family = "JetBrains Mono", weight = "Medium" },
	{ family = "FiraCode Nerd Font", weight = "Medium" },
	{ family = "JetBrainsMono Nerd Font", weight = "Medium" },
})
-- config.window_background_opacity = 0.85
config.term = "xterm-256color"
config.ssh_domains = ssh_domains
config.audible_bell = "Disabled"
-- config.debug_key_events = true
config.status_update_interval = 1000
config.front_end = "WebGpu"
-- config.webgpu_power_preference = "HighPerformance"
config.enable_wayland = true
config.window_background_image = config_dir .. "/arcolinux_dark.jpg"
-- config.window_background_image = config_dir .. "/arcolinux_darkhard.jpg"
config.window_background_image_hsb = { brightness = 0.4 }
-- config.cursor_blink_ease_in = "Constant"
-- config.cursor_blink_ease_out = "Constant"
-- config.default_cursor_style = "BlinkingBlock"

-- NOTE: this over rides the nvim-cmp keybindings
--
-- -- smart splits plugin to use with wezterm and nvim
-- local smart_splits = wezterm.plugin.require("http://github.com/mrjones2014/smart-splits.nvim")
--
-- smart_splits.apply_to_config(config, {
-- 	-- the default config is here, if you'd like to use the default keys,
-- 	-- you can omit this configuration table parameter and just use
-- 	-- smart_splits.apply_to_config(config)
--
-- 	-- directional keys to use in order of: left, down, up, right
-- 	direction_keys = { "h", "j", "k", "l" },
-- 	-- modifier keys to combine with direction_keys
-- 	modifiers = {
-- 		move = "CTRL", -- modifier to use for pane movement, e.g. CTRL+h to move left
-- 		resize = "META", -- modifier to use for pane resize, e.g. META+h to resize to the left
-- 	},
-- })

return config

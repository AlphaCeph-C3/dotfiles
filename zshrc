export TERM="xterm-256color"

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi



# Customize to your needs...

# Aliases
alias untar='tar -zxvf' # Unpack .tar file
alias wget='wget -c' # Download and resume


export HISTORY_SUBSTRING_SEARCH_PREFIXED=true

# For unique history command
export HISTORY_SUBSTRING_SEARCH_ENSURE_UNIQUE=true


# You may need to manually set your language environment
export LANG=en_US.UTF-8

#if [[ -f "${HOME}/.zplug/init.zsh" ]]; then
#  source "${HOME}/.zplug/init.zsh"
#fi

##
## Executes commands at the start of an interactive session.
##
## Authors:
##  Sorin Ionescu <sorin.ionescu@gmail.com>
##

## Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('${HOME}/mambaforge/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "${HOME}/mambaforge/etc/profile.d/conda.sh" ]; then
        . "${HOME}/mambaforge/etc/profile.d/conda.sh"
    else
        export PATH="${HOME}/mambaforge/bin:$PATH"
    fi
fi
unset __conda_setup

if [ -f "${HOME}mambaforge/etc/profile.d/mamba.sh" ]; then
    . "${HOME}/mambaforge/etc/profile.d/mamba.sh"
fi
# <<< conda initialize <<<



# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh


# zplug in for the zsh
# run the init file of zplug/ source zplug
if [[ -f "${HOME}/.zplug/init.zsh" ]]; then
  source "${HOME}/.zplug/init.zsh"
fi
# source "${HOME}/.zplug/init.zsh"

# zplug "zplug/zplug", hook-build:"zplug --self-manage"

# clear packages
# zplug clear

# Plugins
zplug "plugins/git",   from:oh-my-zsh
zplug "Junker/zsh-archlinux"
zplug "zsh-users/zsh-autosuggestions"
#zplug "junegunn/fzf"
zplug "g-plane/zsh-yarn-autocompletions"
zplug "MichaelAquilina/zsh-you-should-use"

# zplug "Peltoche/lsd"
# zplug "plugins/osx",   from:oh-my-zsh
# zplug "clvv/fasd"
# zplug "laggardkernel/zsh-thefuck"
# zplug "b4b4r07/enhancd", use:init.sh
# zplug "romkatv/powerlevel10k", as:theme, depth:1

zplug load


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

export FZF_DEFAULT_COMMAND="fd --type f"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_CTRL_T_OPTS="
  --preview 'bat -n --color=always {}'
  --bind 'ctrl-/:change-preview-window(down|hidden|)'"
export FZF_ALT_C_OPTS="--preview 'tree -C {}'"

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=121'
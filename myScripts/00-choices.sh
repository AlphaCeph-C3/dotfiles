#!/bin/bash
#set -e
##################################################################################################################
# Author    : Harish Kukka
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################
#tput setaf 0 = black
#tput setaf 1 = red
#tput setaf 2 = green
#tput setaf 3 = yellow
#tput setaf 4 = dark blue
#tput setaf 5 = purple
#tput setaf 6 = cyan
#tput setaf 7 = gray
#tput setaf 8 = light blue
##################################################################################################################

installed_dir=$(dirname $(readlink -f $(basename `pwd`)))

##################################################################################################################

# After a clean system install!

echo
echo "Pacman parallel downloads if needed -for ArcoLinux"
FIND="ParallelDownloads = 8"
REPLACE="ParallelDownloads = 20"
sudo sed -i "s/$FIND/$REPLACE/g" /etc/pacman.conf

echo
echo "Pacman parallel downloads if needed - for Arch Linux"
FIND="#ParallelDownloads = 5"
REPLACE="ParallelDownloads = 20"
sudo sed -i "s/$FIND/$REPLACE/g" /etc/pacman.conf

echo
tput setaf 3
echo "################################################################"
echo "################### Start your choices"
echo "################################################################"
tput sgr0
echo

sudo pacman -Sy

sh 100-install-core-software.sh

tput setaf 3
echo "################################################################"
echo "End current choices"
echo "################################################################"
tput sgr0